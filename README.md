# AutoCompleteDemo
This is a simple input box to accept commemts and allow search of a user by userName or name, and displays the avatar.  Only displays in a list view, and would want to make it look nicer with a closer layut to the cursor location. Also would want to allow for selection of another user after the first is selected

## To Run Demo
First Install packages and dependencies `npm install`

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
