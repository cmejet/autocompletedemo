import { Component, OnInit } from '@angular/core';
import User from '../../models/user';
import MockData from '../../models/UserData';
import LookupHelper from '../../helpers/lookup.helper';

@Component({
  selector: 'app-auto-complete',
  templateUrl: './auto-complete.component.html',
  styleUrls: ['./auto-complete.component.css']
})
export class AutoCompleteComponent implements OnInit {
  public comments: string;
  public searchValue: string;
  public users: User[];
  public userResults: User[];
 
  constructor() {

   }

  ngOnInit() {
    this.users = User.parseArray(MockData.data());
  }

  public onChange(searchValue: string): void {
   this.searchValue = LookupHelper.parseSearch(searchValue);
   this.userResults = LookupHelper.findUser(this.searchValue, this.users);
  }
  public selectUser(selected: User): void {
    const ix = this.comments.indexOf('@');
    this.comments = this.comments.substring(0 , ix + 1).concat(selected.userName);
    this.userResults = [];
  }

}
