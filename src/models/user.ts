export default class User {
    public userName: string;
    public avatarUrl: string;
    public name: string;
    constructor() {

    }

    public static parse(json: any): User {
        const item = new User();
        item.userName = json.username;
        item.avatarUrl = json.avatar_url;
        item.name = json.name;
        return item;
    }

    public static parseArray(json: any[]): User[] {
       return json.map(this.parse);
    }
}
