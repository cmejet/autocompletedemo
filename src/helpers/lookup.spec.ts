import User from '../models/user';
import MockData from '../models/UserData';
import LookupHelper from '../helpers/lookup.helper';

describe('Lookup Helper', () => {
    const   mock = MockData.data();
    let users = [];
    beforeAll(() => {
        users = User.parseArray(mock);
    });

    describe('findUser', () => {
        it('should return empty array if no value passed in', () => {
            const result = LookupHelper.findUser('', users);
            expect(result.length).toBe(0);
        });
        
        it('should return empty array if no match for userName', () => {
            const result = LookupHelper.findUser('@', users);
            expect(result.length).toBe(0);
        });
        it('should return empty array if no match for name', () => {
            const result = LookupHelper.findUser('@', users);
            expect(result.length).toBe(0);
        });
        it('should return array of values matching search term in userName', () => {
            const result = LookupHelper.findUser('ptur', users);
            expect(result.length).toBe(1);
            expect(result[0].userName).toBe('pturner0');
        });
        it('should return array of values matching search term in userName regardless of casing', () => {
            const result = LookupHelper.findUser('pTuRn', users);
            expect(result.length).toBe(1);
            expect(result[0].userName).toBe('pturner0');
        });
        it('should return array of values matching search term in userName', () => {
            const result = LookupHelper.findUser('Paula', users);
            expect(result.length).toBe(2);
            expect(result[0].name).toBe('Paula Turner');
            expect(result[0].userName).toBe('pturner0');
            expect(result[1].name).toBe('Paula Romero');
        });
        it('should return array of values matching search term in userName regardless of casing', () => {
            const result = LookupHelper.findUser('pAula', users);
            expect(result.length).toBe(2);
            expect(result[0].name).toBe('Paula Turner');
            expect(result[0].userName).toBe('pturner0');
            expect(result[1].name).toBe('Paula Romero');
        });
        it('should return first 15 results', () => {
            const result = LookupHelper.findUser('p', users);
            expect(result.length).toBe(15);
        });
    });
    describe('parse search', () => {
        it('should parse search terms to give username', () => {
            expect(LookupHelper.parseSearch('this is my comment to @pt')).toBe('pt');
            expect(LookupHelper.parseSearch('this is my comment to @paula t')).toBe('paula t');
        });
        it('should return empty string if no @', () => {
            expect(LookupHelper.parseSearch('this is my comment to')).toBe('');
        });
    });
});
