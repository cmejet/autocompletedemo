import User from '../models/user';

export default class LookupHelper {

    public static findUser(searchValue: string, list: User[]): User[] {
        if(!searchValue.length) return [];
        searchValue = searchValue.toUpperCase();
 return list.filter((u: User) => u.userName.toUpperCase().indexOf(searchValue) > -1
    || u.name.toUpperCase().indexOf(searchValue) > -1 ).slice(0, 15);
    }

    public static parseSearch(searchValue: string): string {
        const ix = searchValue.indexOf('@');
        if(ix === -1) return '';
        
        return searchValue.substring(ix + 1, searchValue.length);
    }
}
